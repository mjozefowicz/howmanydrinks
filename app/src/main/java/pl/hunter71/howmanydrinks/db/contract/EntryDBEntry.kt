package pl.hunter71.howmanydrinks.db.contract

import android.provider.BaseColumns


object EntryDBEntry : BaseColumns, ITable {

    override val TABLE_NAME: String = "entries"

    object Column {
        val ID = "id"
        var DATE = "date"
        var TIME = "time"
        var QUANTITY = "quantity"
        var DESCRIPTION = "description"
        var ARCHIVED = "archived"
    }

    object DataType {
        val ID = "INTEGER PRIMARY KEY AUTOINCREMENT"
        var DATE = "INTEGER NOT NULL"
        var TIME = "INTEGER"
        var QUANTITY = "INTEGER NOT NULL"
        var DESCRIPTION = "TEXT NOT NULL"
        var ARCHIVED = "INTEGER NOT NULL"
    }

    override val CREATE_TABLE_SQL: String = (
        "CREATE TABLE $TABLE_NAME ("
            + "${Column.ID} ${DataType.ID}"
            + ", ${Column.DATE} ${DataType.DATE}"
            + ", ${Column.TIME} ${DataType.TIME}"
            + ", ${Column.QUANTITY} ${DataType.QUANTITY}"
            + ", ${Column.DESCRIPTION} ${DataType.DESCRIPTION}"
            + ", ${Column.ARCHIVED} ${DataType.ARCHIVED}"
        + ")"
    )

    override val DROP_TABLE_SQL: String = "DROP TABLE IF EXISTS ${TABLE_NAME}"
}

package pl.hunter71.howmanydrinks.db.model

import android.content.ContentValues


interface IModelObject {
    var id: Int
    var archived: Boolean

    fun tableName(): String
    fun idColumn(): String
    fun values(): ContentValues
}

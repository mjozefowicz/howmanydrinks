package pl.hunter71.howmanydrinks.db

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import pl.hunter71.howmanydrinks.db.contract.*
import pl.hunter71.howmanydrinks.db.model.Entry
import pl.hunter71.howmanydrinks.db.model.IModelObject
import pl.hunter71.howmanydrinks.utils.LoggingTags
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger
import kotlin.collections.ArrayList

class DBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        // If you change the database schema, you must increment the database version.
        val DATABASE_VERSION = 1
        val DATABASE_NAME = "HowManyDrinks_${DATABASE_VERSION}.db"

        val TABLES = setOf<ITable>(
            EntryDBEntry
        )
    }

    override fun onCreate(db: SQLiteDatabase) {
        for (t: ITable in TABLES) {
            var query: String = t.CREATE_TABLE_SQL

            db.execSQL(query)
            Log.d(LoggingTags.SQL.tag, "Run query: $query")
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        TODO("not implemented")
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        TODO("not implemented")
    }

    @Throws(SQLiteConstraintException::class)
    fun selectEntriesByDate(timestamp: Int): ArrayList<Entry> {
        // Gets the data repository in readonly mode
        val db = readableDatabase

        var query: String = "SELECT * FROM ${EntryDBEntry.TABLE_NAME}" +
                            " WHERE NOT ${EntryDBEntry.Column.ARCHIVED}" +
                            " AND ${EntryDBEntry.Column.DATE} = ?"
        var args = arrayOf(timestamp.toString())

        val entries = ArrayList<Entry>()
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(query, args)
        } catch (e: SQLiteException) {
            Log.e(LoggingTags.SQL.tag, "Error with query $query: ${e.toString()}")
            return entries
        }

        if (cursor.moveToFirst()) {
            do {
                entries.add(getEntry(cursor))
            } while (cursor.moveToNext())
        }

        Log.d(LoggingTags.SQL.tag, "Selected entries: ${entries.joinToString(",")}")

        db.close()
        return entries
    }

    private fun getEntry(cursor: Cursor): Entry {
        var entry = Entry()

        entry.id = getInt(cursor, EntryDBEntry.Column.ID)
        entry.date = getInt(cursor, EntryDBEntry.Column.DATE)
        entry.time = getInt(cursor, EntryDBEntry.Column.TIME)
        entry.quantity = getInt(cursor, EntryDBEntry.Column.QUANTITY)
        entry.description = getString(cursor, EntryDBEntry.Column.DESCRIPTION)
        entry.archived = getInt(cursor, EntryDBEntry.Column.ARCHIVED) == 1

        return entry
    }

    private fun getInt(cursor: Cursor, columnName: String): Int {
        return cursor.getInt(cursor.getColumnIndex(columnName))
    }

    private fun getString(cursor: Cursor, columnName: String): String {
        return cursor.getString(cursor.getColumnIndex(columnName))
    }

    @Throws(SQLiteConstraintException::class)
    fun insert(o: IModelObject): Int {
        // Gets the data repository in write mode
        val db = writableDatabase
        var newRowId = -999

        try {
            newRowId = db.insert(o.tableName(), null, o.values()).toInt()
        }
        catch (e: Exception) {
            Log.e(
                LoggingTags.SQL.tag,
                "Error when insert object to table '${o.tableName()}': ${e.toString()}"
            )
        }
        finally {
            Log.d(LoggingTags.SQL.tag, "Inserting values: ${o.values().toString()}")
        }

        db.close()
        return newRowId
    }

    @Throws(SQLiteConstraintException::class)
    fun update(o: IModelObject): Int {
        // Gets the data repository in write mode
        val db = writableDatabase
        var rowId = -997

        try {
            rowId = db.update(
                o.tableName(),
                o.values(),
                o.idColumn() + "=?", arrayOf(o.id.toString())
            )
        }
        catch (e: Exception) {
            Log.e(
                LoggingTags.SQL.tag,
                "Error when updating object in table '${o.tableName()}': ${e.toString()}"
            )
        }
        finally {
            Log.d(LoggingTags.SQL.tag, "Inserting values: ${o.values().toString()}")
        }

        db.close()
        return rowId
    }

    @Throws(SQLiteConstraintException::class)
    fun delete(o: IModelObject): Int {
        o.archived = true
        return update(o)
    }

    fun clear(t: ITable) {
        val db = writableDatabase

        var query = "DELETE FROM ${t.TABLE_NAME}"

        db.execSQL(query)
        Log.d(LoggingTags.SQL.tag, "Run query: $query")

        db.close()
    }

    fun backup() {
        var dbName = File(DATABASE_NAME).nameWithoutExtension
        var dbFileExtension = File(DATABASE_NAME).extension
        var backupDirectory = File(readableDatabase.path).parentFile

        var dateFormat = "yyyyMMdd"
        var datetimeFormat = "${dateFormat}-HHmmss"
        var separator = "__"

        fun backupFilename(): String {
            return "${dbName}${separator}${getCurrentDateTime(datetimeFormat)}.${dbFileExtension}"
        }

        fun isTodayBackup(): Boolean {
            val backupFilePrefix = "${dbName}${separator}${getCurrentDateTime(dateFormat)}"

            return backupDirectory.walk().any {f -> f.name.startsWith(backupFilePrefix)}
        }

        if (isTodayBackup()) {
            Log.d(LoggingTags.Backup.tag, "Backup already exists.")
            return
        }
        else {
            val backupFilePath = "${backupDirectory}${File.separator}${backupFilename()}"
            copyDatabase(backupFilePath)
        }
    }

    private fun getCurrentDateTime(format: String): String {
        return SimpleDateFormat(format).format(Date())
    }

    private fun copyDatabase(backupFilePath: String): Boolean {
        try {
            copyFile(readableDatabase.path, backupFilePath)
        }
        catch (e: IOException) {
            Log.e(LoggingTags.Backup.tag, "Backup failed.")
            return false
        }

        Log.d(LoggingTags.Backup.tag, "Backup succeed to file: $backupFilePath")
        return true
    }

    private fun copyFile(input: String, output: String) {
        val inputStream = FileInputStream(input)
        val outputStream = FileOutputStream(output)
        val buffer = ByteArray(1024)

        while (inputStream.read(buffer) > 0) {
            outputStream.write(buffer)
        }

        inputStream.close()
        outputStream.flush()
        outputStream.close()
    }
}

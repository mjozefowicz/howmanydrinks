package pl.hunter71.howmanydrinks.db.model

import android.content.ContentValues
import pl.hunter71.howmanydrinks.db.contract.EntryDBEntry


class Entry: IModelObject {

    companion object {
        const val DATE_FORMAT: String = "dd-MM-yyyy"
    }

    constructor(description: String, quantity: Int, date: Int, time: Int = 0) {
        this.description = description
        this.quantity = quantity
        this.date = date
        this.time = time
    }
    constructor()
        : this("", 0, 19700101)

    // *, int, pk, autoincrement
    override var id: Int = 0
    // *, int (year * 10000 + month * 100 + day)
    var date: Int = 0
    // int, [min]
    var time: Int = 0
    // *, int
    var quantity: Int = 0
    // *, nvarchar255
    var description: String = ""
        set(value) {
            field = value.trim()
        }
    // *, tinyint
    override var archived: Boolean = false

    override fun toString(): String {
        return "<id=${id} (${date} ${time}): ${description} -> ${quantity}ml>"
    }

    override fun idColumn(): String {
        return EntryDBEntry.Column.ID
    }

    override fun tableName(): String {
        return EntryDBEntry.TABLE_NAME
    }

    override fun values(): ContentValues {
        return ContentValues().apply {
            put(EntryDBEntry.Column.DATE, date)
            put(EntryDBEntry.Column.TIME, time)
            put(EntryDBEntry.Column.QUANTITY, quantity)
            put(EntryDBEntry.Column.DESCRIPTION, description)
            put(EntryDBEntry.Column.ARCHIVED, archived)
        }
    }
}

package pl.hunter71.howmanydrinks.utils

import java.text.SimpleDateFormat
import java.util.*


class SimpleDate(year: Int, month: Int, day: Int) {
    var year: Int = year
    var month: Int = month
    var day: Int = day

    companion object {
        const val DATE_FORMAT = "yyyy-MM-dd"

        fun fromTimestamp(timestamp: Int): SimpleDate {
            var year: Int = timestamp / 10000
            var month: Int = (timestamp / 100).rem(100)
            var day: Int = timestamp.rem(100)

            return SimpleDate(year, month, day)
        }

        fun fromDate(date: Date): SimpleDate {
            var dateElements: List<String> = stripDate(date)
            
            var year: Int = dateElements[0].toInt()
            var month: Int = dateElements[1].toInt()
            var day: Int = dateElements[2].toInt()

            return SimpleDate(year, month, day)
        }

        fun fromString(date: String, format: String = DATE_FORMAT): SimpleDate {
            return fromDate(SimpleDateFormat(format).parse(date))
        }

        protected fun stripDate(date: Date): List<String> {
            return SimpleDateFormat(DATE_FORMAT).format(date).split("-")
        }
    }

    fun toTimestamp(): Int {
        return year * 10000 + month * 100 + day
    }

    override fun toString(): String {
        var strMonth: String = if (month < 10) "0$month" else "$month"
        var strDay: String = if (day < 10) "0$day" else "$day"

        return "$year-$strMonth-$strDay"
    }
}

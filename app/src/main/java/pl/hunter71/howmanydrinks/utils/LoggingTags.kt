package pl.hunter71.howmanydrinks.utils

enum class LoggingTags(var tag: String) {
    MainActivity_OnCreate("[MainActivity:onCreate]"),

    SQL("[SQL]"),
    Backup("[Backup]"),
}

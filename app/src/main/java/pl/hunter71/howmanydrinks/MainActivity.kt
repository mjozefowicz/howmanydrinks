package pl.hunter71.howmanydrinks

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import pl.hunter71.howmanydrinks.db.DBHelper
import pl.hunter71.howmanydrinks.db.model.Entry
import pl.hunter71.howmanydrinks.utils.LoggingTags
import pl.hunter71.howmanydrinks.utils.SimpleDate
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    val ACTION_HIDE = "hide"
    val ACTION_SHOW = "show"

    val DATE_FORMAT = "dd-MM-yyyy"
    val TARGET_VOLUME: Double = 2.0

    private lateinit var textMessage: TextView
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                textMessage.setText(R.string.title_home)

                initEntriesListView()

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                textMessage.setText(R.string.title_dashboard)

                initAddEntryView()

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                textMessage.setText(R.string.title_notifications)

                controlsHome(ACTION_HIDE)
                controlsDashboard(ACTION_HIDE)

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        textMessage = findViewById(R.id.message)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        backup()
        initEntriesListView()
    }

    override fun onResume() {
        super.onResume()

        initEntriesListView()
    }

    private fun initAddEntryView() {
        refreshInputs()

        controlsHome(ACTION_HIDE)
        controlsDashboard(ACTION_SHOW)
    }

    private fun initEntriesListView() {
        // target textView
        textView_target.text = "Cel na dziś:   ${TARGET_VOLUME} l \uD83C\uDF7A"

        val entries = getEntries()
        val volume = calcVolume(entries)

        refreshEntries(entries)
        refreshProgress(volume)

        controlsDashboard(ACTION_HIDE)
        controlsHome(ACTION_SHOW)
    }

    private fun calcVolume(entries: ArrayList<Entry>): Double {
        return entries.sumBy { entry -> entry.quantity } / 1000.0
    }

    private fun getEntries(): ArrayList<Entry> {
        var today: Int = SimpleDate.fromDate(Date()).toTimestamp()
        return DBHelper(this).selectEntriesByDate(today)
    }

    private fun refreshEntries(entries: ArrayList<Entry>) {
        // descriptions display textView
        textView_list.setMovementMethod(ScrollingMovementMethod());
        textView_list.text = entries.map { x -> x.description }.joinToString("\n")

        // volumes display textView
        textView_volumes.text = entries.map { x ->
            if (x.quantity < 500) "${x.quantity} ml" else "${x.quantity / 1000.0} l"
        }.joinToString("\n")
    }

    private fun refreshProgress(volume: Double) {
        val progress = (volume / TARGET_VOLUME * 100).toInt()

        var progressText = "RAZEM: ${volume} l   (${progress}%)"
        var progressBarColor = ColorStateList.valueOf(Color.RED)

        if (progress > 100) {
            progressText += (" \uD83C\uDFC5")
            progressBarColor = ColorStateList.valueOf(Color.GREEN)
        }

        // progress textView
        textView_progress.text = progressText
        // score progressBar
        progressBar_score.progress = progress
        progressBar_score.setProgressTintList(progressBarColor)

        Log.d(LoggingTags.MainActivity_OnCreate.tag, "Progress: ${progress}%")
    }

    private fun refreshInputs() {
        // title textView
        textView_title.text = "Właśnie wypiłam:"

        // date editText
        editText_date.setText(SimpleDateFormat(DATE_FORMAT).format(Date()))

        // values for spinner selections
        var quantitiesList: List<Int> = resources.getStringArray(R.array.volumes)
            .map { x -> x.toInt() }

        // add button
        button_add.setOnClickListener {
            if (
                editText_date.text.isNotBlank()
                && editText_description.text.isNotBlank()
                && spinner_volume.selectedItemPosition > 0
            ) {
                var dateText: String = editText_date.text.toString()
                var date: Int = SimpleDate.fromString(dateText, DATE_FORMAT).toTimestamp()
                var description: String = editText_description.text.toString()
                var quantity: Int = quantitiesList[spinner_volume.selectedItemPosition]

                var entry = Entry(description, quantity, date)
                DBHelper(this).insert(entry)

                afterAddEntryCleanup()
            }
        }
    }

    private fun afterAddEntryCleanup() {
        editText_description.setText("")
        spinner_volume.setSelection(0)
    }

    private fun controlsHome(action: String) {
        val controls = arrayListOf<View>(
            textView_target
            , textView_list
            , textView_volumes
            , textView_progress
            , progressBar_score
        )
        when(action) {
            ACTION_SHOW -> show(controls)
            ACTION_HIDE -> hide(controls)
        }
    }

    private fun controlsDashboard(action: String) {
        val controls = arrayListOf<View>(
            textView_title
            , editText_date
            , editText_description
            , spinner_volume
            , button_add
        )
        when(action) {
            ACTION_SHOW -> show(controls)
            ACTION_HIDE -> hide(controls)
        }
    }

    private fun show(controls: ArrayList<View>) {
        controls.forEach { x -> x.visibility = View.VISIBLE }
    }

    private fun hide(controls: ArrayList<View>) {
        controls.forEach { x -> x.visibility = View.INVISIBLE }
    }

    private fun backup() {
        DBHelper(this).backup()
    }
}
